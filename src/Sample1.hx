import util.Free;
import util.Id;
import util.Unit;
import util.NaturalTransformation;

using util.Functions;
using util.FreeApi;

enum Interact<A> {
	Ask(prompt:String):Interact<String>;
	Tell(msg:String):Interact<Unit>;
}

@:publicFields
class InteractApi {
	static function ask (prompt) return FreeApi.lift(Ask(prompt));
	static function tell (msg) return FreeApi.lift(Tell(msg));
}

// Interactive Console interpretation of the Program
class ConsoleInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	public function new () {}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				Id.lift(Sys.stdin().readLine());
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

// interpretation based on a Map with predefined Values
class MapInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	var answers = null;

	public function new (answers:Map<String, String>) {
		this.answers = answers;
	}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				var a = answers[msg];
				Id.lift(a == null ? "<<Unknown>>" : a);
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

class Sample1 {
	static function main () {

		function createProgram () {
			return InteractApi.ask("What's your first name?").flatMap( first -> {
				return InteractApi.ask("What's your last name?").flatMap( last -> {
					return if (first == "" || last == "") {
						InteractApi.tell('first and last should not be empty!').flatMap( _ -> createProgram());
					} else {
						InteractApi.tell('Hello $first, $last!');
					}
				});
			});
		}

		var program = createProgram();


		// run map interpretation
		var map = [
			"What's your first name?" => "Peter",
			"What's your last name?" => "Watson",
		];
		FreeApi.interpret(program, new MapInterpretation(map), new IdMonad());

		// run interactive console interpretation
		FreeApi.interpret(program, new ConsoleInterpretation(), new IdMonad());
	}
}