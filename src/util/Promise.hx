package util;

import haxe.ds.Option;

class Promise<T> {
	var value:Option<T>;

	var listeners:Array<T->Void>;
	function new () {
		listeners = [];
		value = None;
	}

	function notify (v:T) {
		for (l in listeners.copy()) {
			l(v);
		}
		listeners = [];
	}

	function addListener (f:T->Void) {
		switch value {
			case Some(v): f(v);
			case _:
				listeners.push(f);
		}
	}

	function setComplete(v:T) {
		this.value = Some(v);
		notify(v);
	}
}

@:access(util.Promise)
class PromiseApi {

	public static function create <T>(f:(T->Void)->Void):Promise<T> {
		var p = new Promise();
		f( x -> p.setComplete(x));
		return p;
	}
	public static function flatMap <S,T>(a:Promise<S>, f:S->Promise<T>):Promise<T> {
		return create(resolve -> {
			a.addListener(x -> {
				f(x).addListener(resolve);
			});
		});
	}

	public static function map <S,T>(a:Promise<S>, f:S->T):Promise<T> {
		return create(resolve -> {
			a.addListener(x -> {
				resolve(f(x));
			});
		});
	}

	public static function pure <T>(a:T):Promise<T> {
		return create(resolve -> {
			resolve(a);
		});
	}
}

class PromiseMonad implements Monad<Promise<_>> {
	public function new () {}
	public function pure<A>(a: A): Promise<A> {
		return PromiseApi.pure(a);
	}
    public function flatMap<A,B>(m: Promise<A>, f: A -> Promise<B>):Promise<B> {
		return PromiseApi.flatMap(m, f);
	}
}