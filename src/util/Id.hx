package util;
abstract Id<T>(T) {
	public inline static function lift <T>(t:T):Id<T> return cast t;
	public inline function runIdentity ():T return this;
}

class IdMonad implements Monad<Id<_>> {
	public function new () {}
	public inline function pure<A> (a:A) return Id.lift(a);
	public inline function flatMap<A, B> (fa:Id<A>, f:A->Id<B>) return f(fa.runIdentity());
}