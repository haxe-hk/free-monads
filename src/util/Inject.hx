package util;
import haxe.ds.Option;
import haxe.ds.Either;
interface Inject<F, G> {
	function inj<A>(sub:F<A>):G<A>;
	function prj<A>(sup:G<A>):Option<F<A>>;
}

class InjectRel<F> implements Inject<F, F> {
	@:implicit public static function instance<F>():Inject<F,F> return new InjectRel();
	public function new () {}
	public inline function inj<A>(sub: F<A>) return sub;
	public inline function prj<A>(sup: F<A>) return Some(sup);
}

class InjectLeft<F,G> implements Inject<F, Coproduct<F, G, _>> {
	@:implicit public static function instance<F,G>():InjectLeft<F, G> return new InjectLeft();
	public function new () {}
	public inline function inj<A>(sub: F<A>) return Coproduct.mk(Left(sub));
	public inline function prj<A>(sup: Coproduct<F, G, A>) return switch sup.run() {
		case Left(fa): Some(fa);
		case Right(_): None;
	}
}

class InjectRight<F, G, H> implements Inject<F, Coproduct<H, G, _>> {
	@:implicit public static function instance<F,G,H>(I:Inject<F, G>):Inject<F, Coproduct<H, G, _>> return new InjectRight(I);
	private var I:Inject<F, G>;
	public function new (I) {
		this.I = I;
	}
	public inline function inj<A>(sub: F<A>) return Coproduct.mk(Right(I.inj(sub)));
	public inline function prj<A>(sup: Coproduct<H,G,A>) return switch sup.run() {
		case Left(_): None;
		case Right(x): I.prj(x);
	}
}
/*
object Inject {
  implicit def injRefl[F[_]] = new Inject[F,F] {
    def inj[A](sub: F[A]) = sub
    def prj[A](sup: F[A]) = Some(sup)
  }

  implicit def injLeft[F[_],G[_]] = new Inject[F,({type λ[α] = Coproduct[F,G,α]})#λ] {
    def inj[A](sub: F[A]) = Coproduct(Left(sub))
    def prj[A](sup: Coproduct[F,G,A]) = sup.run match {
      case Left(fa) => Some(fa)
      case Right(_) => None
    }
  }

  implicit def injRight[F[_],G[_],H[_]](implicit I: Inject[F,G]) =
    new Inject[F,({type f[x] = Coproduct[H,G,x]})#f] {
      def inj[A](sub: F[A]) = Coproduct(Right(I.inj(sub)))
      def prj[A](sup: Coproduct[H,G,A]) = sup.run match {
        case Left(_) => None
        case Right(x) => I.prj(x)
    }
  }
}
*/