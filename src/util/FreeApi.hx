package util;

using util.Functions;

@:publicFields
class FreeApi {
	static function flatMap<F, A, B>(fa:Free<F, A>, f:A->Free<F, B>):Free<F, B> {
		return switch fa {
			case Pure(a): f(a);
			case Bind(fg, g): Bind(fg, g.andThen( x -> flatMap(x, f)) );
		}
	}

	static function map<F, A, B>(fa:Free<F, A>, f:A->B):Free<F, B> {
		return switch fa {
			case Pure(a): Pure(f(a));
			case Bind(fg, g): Bind(fg, g.andThen( x -> map(x, f)));
		}
	}

	static function interpret <G, F, A>(fa:Free<F, A>, translate: NaturalTransformation<F, G>, monad:Monad<G> ) {
		return switch fa {
			case Pure(a): monad.pure(a);
			case Bind(fa, g): monad.flatMap(translate.apply(fa), x -> interpret(g(x), translate, monad));
		}
	}

	static function lift <F, A>(fa:F<A>):Free<F, A> {
		return Bind(fa, a -> Pure(a));
	}

	static function liftI<F,G,A>(f: F<A>, I: Inject<F,G>): Free<G,A> {
		return Bind(I.inj(f), (a:A) -> Pure(a));
	}


}