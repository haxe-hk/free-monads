package util;
import haxe.ds.Either;
abstract Coproduct<F,G,A>(Either<F<A>, G<A>>) {
	public inline function new (x) this = x;
	public static inline function mk <F,G,A>(e:Either<F<A>, G<A>>) return new Coproduct(e);
	public inline function run () return this;
}
//case class Coproduct[F[_],G[_],A](run: Either[F[A],G[A]])