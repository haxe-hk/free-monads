package util;
interface NaturalTransformation<F, G> {
	function apply<A>(f:F<A>):G<A>;
}

class OrNaturalTransformation<F,H,G> implements NaturalTransformation<Coproduct<F, H, _>, G> {
	var fg = null;
	var hg = null;
	public function new (fg:NaturalTransformation<F, G>, hg:NaturalTransformation<H, G>) {
		this.fg = fg;
		this.hg = hg;
	}
	public function apply<A>(c:Coproduct<F,H,A>):G<A> return switch c.run() {
		case Left(fa): fg.apply(fa);
		case Right(ha): hg.apply(ha);
	}
}

class ComposedNaturalTransformation<A, B, C> implements NaturalTransformation<A, C> {

	var a = null;
	var b = null;

	public function new (a:NaturalTransformation<A, B>, b:NaturalTransformation<B, C>) {
		this.a = a;
		this.b = b;
	}

	public function apply<X>(fa:A<X>):C<X> {
		return b.apply(a.apply(fa));
	}
}


class NaturalTransformationApi {
	public static function compose<F, G, H>(x:NaturalTransformation<F, G>, f:NaturalTransformation<G, H>) {
		return new ComposedNaturalTransformation(x, f);
	}
	public static function or<F, G, H>(x:NaturalTransformation<F, G>, f:NaturalTransformation<H, G>) {
		return new OrNaturalTransformation(x, f);
	}
}