package util;
enum Free<F, A> {
	Pure(a:A):Free<F, A>;
	Bind<B>(fa:F<A>, f: A -> Free<F, B>):Free<F, B>;
}