package util;
class Functions {
	public static function andThen <A, B, C>(a:A->B, b:B->C):A->C {
		return x -> b(a(x));
	}
}