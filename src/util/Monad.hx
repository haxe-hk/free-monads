package util;
interface Monad<M>{
	function pure<A>(a: A): M<A>;
	function flatMap<A,B>(m: M<A>, f: A -> M<B>):M<B>;
}