class Log {

	macro public static function enable (v:Bool) {
		@:privateAccess haxe.macro.Context.load("enable_type_log", 1)(v);
		return macro null;
	}
}