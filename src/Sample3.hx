import util.Free;
import util.Id;
import util.Unit;
import util.Promise;
import util.NaturalTransformation;

using util.Functions;
using util.FreeApi;

enum ProgramInstruction<A> {
	InteractInstruction(a:Interact<A>):ProgramInstruction<A>;
	HttpInstruction(a:Http<A>):ProgramInstruction<A>;
}

enum Interact<A> {
	Ask(prompt:String):Interact<String>;
	Tell(msg:String):Interact<Unit>;
}

typedef Program<T> = Free<ProgramInstruction<_>, T>;

@:publicFields
class InteractApi {
	private static function lift <T>(i:Interact<T>) return FreeApi.lift(InteractInstruction(i));

	static function ask (prompt) return lift(Ask(prompt));
	static function tell (msg) return lift(Tell(msg));
}

typedef UserRegistry = Array<{ first : String, last : String }>;

enum Http<A> {
	IsUserRegistered(first:String, last:String):Http<Bool>;
	Register(first:String, last:String):Http<Bool>;
	GetAllUsers:Http<UserRegistry>;
	Nothing:Http<Unit>;
}

@:publicFields
class HttpApi {
	private static function lift <T>(i:Http<T>)  return FreeApi.lift(HttpInstruction(i));

	static function register (first, last) return lift(Register(first, last));
	static function getAllUsers () return lift(GetAllUsers);
	static function isUserRegistered (first, last) return lift(IsUserRegistered(first, last));
}

// Interactive Console interpretation of the Program
class ConsoleInteractInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	public function new () {}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				Id.lift(Sys.stdin().readLine());
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

class IdToPromiseTransformation implements NaturalTransformation<Id<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Id<A>):Promise<A> {
		return PromiseApi.pure(f.runIdentity());
	}
}



class ProgramInterpretation implements NaturalTransformation<ProgramInstruction<_>, Promise<_>> {

	var i1 = null;
	var i2 = null;

	public function new (i1:NaturalTransformation<Interact<_>, Promise<_>>, i2:NaturalTransformation<Http<_>, Promise<_>>) {
		this.i1 = i1;
		this.i2 = i2;
	}

	public function apply<A>(f:ProgramInstruction<A>):Promise<A> {
		return switch (f) {
			case InteractInstruction(a):
				i1.apply(a);
			case HttpInstruction(a):
				i2.apply(a);

		}

	}
}

class HttpInterpretation implements NaturalTransformation<Http<_>, Promise<_>> {

	var users:UserRegistry;
	public function new () {
		this.users = [];
	}

	public function apply<A>(f:Http<A>):Promise<A> {
		return switch f {
			case IsUserRegistered(first, last):
				var registered = users.filter(x -> x.first == first && x.last == last).length > 0;
				PromiseApi.pure(registered);
			case GetAllUsers:
				PromiseApi.pure(users);
			case Register(first, last):
				users.push({ first: first, last: last });
				PromiseApi.pure(true);
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}

class Sample3 {
	static function main () {
		var interactInterp = NaturalTransformationApi.compose(new ConsoleInteractInterpretation(), new IdToPromiseTransformation());
		var httpInterp = new HttpInterpretation();
		var programInterp = new ProgramInterpretation(interactInterp, httpInterp);


		function askYesNo (question, ifYes, ifNo):Program<Unit> {
			return InteractApi.ask(question).flatMap(x -> switch x.toLowerCase() {
				case "yes"|"y":
					ifYes();
				case "no"|"n":
					ifNo();
				case x:
					InteractApi.tell("I don't understand " + x).flatMap(_ -> askYesNo(question, ifYes, ifNo));
			});
		}

		function restart (success:Void->Program<Unit>):Program<Unit> {
			return askYesNo("restart?", success, () -> InteractApi.tell('Thx for using this App!'));
		}

		function register (first:String, last:String, success:Void->Program<Unit>):Program<Unit> {
			return
				HttpApi.register(first, last).flatMap(x -> {
					if (x) {
						HttpApi.getAllUsers().flatMap(x -> {
							var allUsers = x.map( e -> e.first + " " + e.last).join(", ");
							InteractApi.tell('allUsers: ' + allUsers).flatMap(_ -> {
								restart(success);
							});
						});
					} else {
						InteractApi.tell('Cannot register, goodbye');
					}
				});
		}

		function askForRegistration (success:Void->Program<Unit>):Program<Unit> {
			return askYesNo('You\'re not registered, want to register?', success, () -> InteractApi.tell('Thx for using this App!'));
		}

		function mainProgram ():Program<Unit> {
			return InteractApi.tell("User Registration App").flatMap( _ -> {
				return InteractApi.ask("What's your first name?").flatMap( first -> {
					return InteractApi.ask("What's your last name?").flatMap( last -> {

						if (first == "" || last == "") {
							InteractApi.tell('first and last should not be empty!').flatMap( _ -> mainProgram());
						} else {
							HttpApi.isUserRegistered(first, last).flatMap( x -> {
								if (x) {
									InteractApi.tell('Hello registered user $first, $last!');
								} else {
									askForRegistration(() -> register(first, last, mainProgram));
								}
							});
						}
					});
				});
			});
		}


		var program = mainProgram();


		// run interactive console interpretation
		FreeApi.interpret(program, programInterp, new PromiseMonad());


	}
}