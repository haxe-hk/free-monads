import util.Free;
import util.Id;
import util.Unit;
import util.Promise;
import util.Inject;
import util.NaturalTransformation;
import util.Coproduct;

import scuts.implicit.Implicit;
import scuts.implicit.Wildcard;

using util.Functions;
using util.FreeApi;

typedef ProgramInstruction<A> = Coproduct<Interact<_>, Http<_>, A>;

enum Interact<A> {
	Ask(prompt:String):Interact<String>;
	Tell(msg:String):Interact<Unit>;
}

@:publicFields
class InteractApi<F> {

	@:implicit static function instance <F>(I:Inject<Interact<_>, F>) return new InteractApi(I);
	var I = null;
	private function new (I:Inject<Interact<_>, F>) {
		this.I = I;
	}

	function ask (prompt) return FreeApi.liftI(Ask(prompt), I);
	function tell (msg) return FreeApi.liftI(Tell(msg), I);
}

enum Http<A> {
	IsUserRegistered(first:String, last:String):Http<Bool>;
	Nothing:Http<Unit>;
}

@:publicFields
class HttpApi<F> {

	@:implicit static function instance <F>(I:Inject<Http<_>, F>) return new HttpApi(I);
	var I = null;
	private  function new (I:Inject<Http<_>, F>) {
		this.I = I;
	}

	function isUserRegistered (first, last) return FreeApi.liftI(IsUserRegistered(first, last), I);
}


enum Log<A> {
	LogMessage(msg:String):Log<Unit>;
	Nothing:Log<Unit>;
}

@:publicFields
class LogApi<F> {

	@:implicit static function instance <F>(I:Inject<Log<_>, F>) return new LogApi(I);
	var L = null;
	private  function new (L:Inject<Log<_>, F>) {
		this.L = L;
	}

	function logMessage (msg) return FreeApi.liftI(LogMessage(msg), L);
}

enum Store<A> {
	PutString(v:String):Store<Unit>;
	Nothing:Store<Unit>;
}

@:publicFields
class StoreApi<F> {

	@:implicit static function instance <F>(I:Inject<Store<_>, F>) return new StoreApi(I);
	var I = null;
	private  function new (I:Inject<Store<_>, F>) {
		this.I = I;
	}

	function putString (val) return FreeApi.liftI(PutString(val), I);
}

// Interactive Console interpretation of the Program
class ConsoleInteractInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	public function new () {}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				Id.lift(Sys.stdin().readLine());
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

class IdToPromiseTransformation implements NaturalTransformation<Id<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Id<A>):Promise<A> {
		return PromiseApi.pure(f.runIdentity());
	}
}

class HttpInterpretation implements NaturalTransformation<Http<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Http<A>):Promise<A> {
		return switch f {
			case IsUserRegistered(first, last):
				PromiseApi.pure(first == "Peter" && last == "Watson");
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}
class StoreInterpretation implements NaturalTransformation<Store<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Store<A>):Promise<A> {
		return switch f {
			case PutString(val):
				trace("save: " + val);
				PromiseApi.pure(Unit);
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}
class LogInterpretation implements NaturalTransformation<Log<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Log<A>):Promise<A> {
		return switch f {
			case LogMessage(msg):
				trace(msg);
				PromiseApi.pure(Unit);
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}

typedef F0<A> = Coproduct<Interact<_>, Log<_>, A>;
typedef F1<A> = Coproduct<Http<_>, F0<_>, A>;
typedef App<A> = Coproduct<Store<_>, F1<_>, A>;

class Sample5 {


	static function askForName <F>(I:Implicit<InteractApi<F>>):Free<F, { first : String, last:String }> {
		return I.ask("What's your first name?").flatMap( first -> {
			return I.ask("What's your last name?").flatMap( last -> {
				return Pure({ first : first, last: last});
			});
		});
	}

	static function checkRegistered <F>(first, last, H:Implicit<HttpApi<F>>, L:Implicit<LogApi<F>>):Free<F, Bool> {
		return L.logMessage("log -> before check user registered").flatMap(_ -> {
			H.isUserRegistered(first, last).flatMap(r -> {
				L.logMessage("log -> after check user registered -> " + r).flatMap(_ -> {
					return Pure(r);
				});
			});
		});
	}

	// composed interact + http program
	static function createComposedProgram <F>(H:Implicit<HttpApi<F>>, I:Implicit<InteractApi<F>>, L:Implicit<LogApi<F>>, S:Implicit<StoreApi<F>>):Free<F, Unit> {
		return askForName(_).flatMap( res -> {
			var first = res.first;
			var last = res.last;

			return if (first == "" || last == "") {
				I.tell('first and last should not be empty!').flatMap( _ -> createComposedProgram(_, _, _, _));
			} else {
				checkRegistered(first, last, _, _).flatMap( x -> {
					return if (x) {
						I.tell('Hello registered user $first, $last!').flatMap(_ -> {
							S.putString('New User $first $last');
						});
					} else {
						I.tell('You\'re not registered!');
					}
				});
			}
		});
	}
	static function main () {
		var storeInterp = new StoreInterpretation();
		var logInterp = new LogInterpretation();
		var interactInterp = NaturalTransformationApi.compose(new ConsoleInteractInterpretation(), new IdToPromiseTransformation());
		var httpInterp = new HttpInterpretation();

		var programInterp = NaturalTransformationApi.or(storeInterp, NaturalTransformationApi.or(httpInterp, NaturalTransformationApi.or(interactInterp, logInterp)));

		var program = createComposedProgram(_,_,_,_);


		// run composed interpretation
		FreeApi.interpret(program, programInterp, new PromiseMonad());

	}
}