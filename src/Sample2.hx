import util.Free;
import util.Id;
import util.Unit;
import util.Promise;
import util.NaturalTransformation;

using util.Functions;
using util.FreeApi;

enum ProgramInstruction<A> {
	InteractInstruction(a:Interact<A>):ProgramInstruction<A>;
	HttpInstruction(a:Http<A>):ProgramInstruction<A>;
}

enum Interact<A> {
	Ask(prompt:String):Interact<String>;
	Tell(msg:String):Interact<Unit>;
}

@:publicFields
class InteractApi {
	private static function lift <T>(i:Interact<T>) return FreeApi.lift(InteractInstruction(i));

	static function ask (prompt) return lift(Ask(prompt));
	static function tell (msg) return lift(Tell(msg));
}

enum Http<A> {
	IsUserRegistered(first:String, last:String):Http<Bool>;
	Nothing:Http<Unit>;
}

@:publicFields
class HttpApi {
	private static function lift <T>(i:Http<T>)  return FreeApi.lift(HttpInstruction(i));

	static function isUserRegistered (first, last) return lift(IsUserRegistered(first, last));
}

// Interactive Console interpretation of the Program
class ConsoleInteractInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	public function new () {}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				Id.lift(Sys.stdin().readLine());
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

class IdToPromiseTransformation implements NaturalTransformation<Id<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Id<A>):Promise<A> {
		return PromiseApi.pure(f.runIdentity());
	}
}



class ProgramInterpretation implements NaturalTransformation<ProgramInstruction<_>, Promise<_>> {

	var i1 = null;
	var i2 = null;

	public function new (i1:NaturalTransformation<Interact<_>, Promise<_>>, i2:NaturalTransformation<Http<_>, Promise<_>>) {
		this.i1 = i1;
		this.i2 = i2;
	}

	public function apply<A>(f:ProgramInstruction<A>):Promise<A> {
		return switch (f) {
			case InteractInstruction(a):
				i1.apply(a);
			case HttpInstruction(a):
				i2.apply(a);

		}

	}
}

class HttpInterpretation implements NaturalTransformation<Http<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Http<A>):Promise<A> {
		return switch f {
			case IsUserRegistered(first, last):

				PromiseApi.pure(first == "Peter" && last == "Watson");
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}

class Sample2 {
	static function main () {
		var interactInterp = NaturalTransformationApi.compose(new ConsoleInteractInterpretation(), new IdToPromiseTransformation());
		var httpInterp = new HttpInterpretation();
		var programInterp = new ProgramInterpretation(interactInterp, httpInterp);


		function createProgram () {
			return InteractApi.ask("What's your first name?").flatMap( first -> {
				return InteractApi.ask("What's your last name?").flatMap( last -> {

					return if (first == "" || last == "") {
						InteractApi.tell('first and last should not be empty!').flatMap( _ -> createProgram());
					} else {
						HttpApi.isUserRegistered(first, last).flatMap( x -> {
							return if (x) {
								InteractApi.tell('Hello registered user $first, $last!');
							} else {
								InteractApi.tell('You\'re not registered!');
							}
						});
					}
				});
			});
		}


		var program = createProgram();


		// run interactive console interpretation
		FreeApi.interpret(program, programInterp, new PromiseMonad());
	}
}