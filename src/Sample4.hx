import util.Free;
import util.Id;
import util.Unit;
import util.Promise;
import util.Inject;
import util.NaturalTransformation;
import util.Coproduct;

import scuts.implicit.Implicit;
import scuts.implicit.Wildcard;

using util.Functions;
using util.FreeApi;

typedef ProgramInstruction<A> = Coproduct<Interact<_>, Http<_>, A>;

enum Interact<A> {
	Ask(prompt:String):Interact<String>;
	Tell(msg:String):Interact<Unit>;
}

@:publicFields
class InteractApi<F> {

	@:implicit static function instance <F>(I:Inject<Interact<_>, F>) return new InteractApi(I);
	var I = null;
	private function new (I:Inject<Interact<_>, F>) {
		this.I = I;
	}

	function ask (prompt) return FreeApi.liftI(Ask(prompt), I);
	function tell (msg) return FreeApi.liftI(Tell(msg), I);
}

enum Http<A> {
	IsUserRegistered(first:String, last:String):Http<Bool>;
	Nothing:Http<Unit>;
}

@:publicFields
class HttpApi<F> {

	@:implicit static function instance <F>(I:Inject<Http<_>, F>) return new HttpApi(I);
	var I = null;
	private  function new (I:Inject<Http<_>, F>) {
		this.I = I;
	}

	function isUserRegistered (first, last) return FreeApi.liftI(IsUserRegistered(first, last), I);
}

// Interactive Console interpretation of the Program
class ConsoleInteractInterpretation implements NaturalTransformation<Interact<_>, Id<_>> {

	public function new () {}

	public function apply<A>(f:Interact<A>):Id<A> {
		return switch f {
			case Ask(msg):
				Sys.println(msg);
				Id.lift(Sys.stdin().readLine());
			case Tell(msg):
				Sys.println(msg);
				Id.lift(Unit);
		}
	}
}

class IdToPromiseTransformation implements NaturalTransformation<Id<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Id<A>):Promise<A> {
		return PromiseApi.pure(f.runIdentity());
	}
}

class HttpInterpretation implements NaturalTransformation<Http<_>, Promise<_>> {

	public function new () {}

	public function apply<A>(f:Http<A>):Promise<A> {
		return switch f {
			case IsUserRegistered(first, last):
				PromiseApi.pure(first == "Peter" && last == "Watson");
			case Nothing:
				PromiseApi.pure(Unit);

		}
	}
}

typedef App<A> = Coproduct<Http<_>, Interact<_>, A>;

class Sample4 {

	static function main () {
		var interactInterp = NaturalTransformationApi.compose(new ConsoleInteractInterpretation(), new IdToPromiseTransformation());
		var httpInterp = new HttpInterpretation();

		var programInterp = NaturalTransformationApi.or(httpInterp, interactInterp);

		// composed interact + http program
		function createComposedProgram <F>(H:Implicit<HttpApi<App<_>>>, I:Implicit<InteractApi<App<_>>>):Free<App<_>, Unit> {
			return I.ask("What's your first name?").flatMap( first -> {
				return I.ask("What's your last name?").flatMap( last -> {

					return if (first == "" || last == "") {
						I.tell('first and last should not be empty!').flatMap( _ -> createComposedProgram(_, _));
					} else {
						H.isUserRegistered(first, last).flatMap( x -> {
							return if (x) {
								I.tell('Hello registered user $first, $last!');
							} else {
								I.tell('You\'re not registered!');
							}
						});
					}
				});
			});
		}

		var program = createComposedProgram(_,_);

		// run composed interpretation
		FreeApi.interpret(program, programInterp, new PromiseMonad());


		// standalone interact program
		function createInteractProgram <F>(I:Implicit<InteractApi<Interact<_>>>):Free<Interact<_>, Unit> {
			return I.ask("What's your first name?").flatMap( first -> {
				return I.ask("What's your last name?").flatMap( last -> {
					I.tell('Hello $first, $last!');
				});
			});
		}

		var program2 = createInteractProgram(_);

		// run interactive console interpretation
		FreeApi.interpret(program2, interactInterp, new PromiseMonad());
		// Id directly
		FreeApi.interpret(program2, new ConsoleInteractInterpretation(), new IdMonad());


	}
}